import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import findLast from "lodash/findLast";
import { LazyImage } from "./LazyImage";
import { formatCurrency } from "../services/utils";
import { store } from "../services/store";
import { ADD_CART_TYPE } from "../reducers/cart";
import { timeout } from "q";

const ItemListContainerOuter = styled.div`
  display: flex;
  flex-direction: column;
`;

const ItemListContainer = styled.div`
  flex: 0;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  height: 100%;
  margin-top: 1em;
  @media only screen and (max-width: 1000px) {
    justify-content: center;
  }

  @media only screen and (max-width: 470px) {
    justify-content: center;
  }
`;

const ItemOuter = styled.div`
  flex: 0 1 calc(100% / 4);
  min-width: 230px;
  max-width: 300px;
  position: relative;
  padding-right: 1em;
  padding-bottom: 1em;
  text-align: center;

  @media only screen and (max-width: 1000px) {
    flex: 0 1 calc(100% / 3);
  }

  @media only screen and (max-width: 470px) {
    flex: 0 1 100%;
  }
`;

const ItemInner = styled.div`
  border: 1px solid var(--primary-color);
  padding: 1em;
  height: 100%;
  display: inline-flex;
  width: 100%;
  flex-direction: column;
  img {
    max-width: 100%;
    height: 150px;
    width: auto;
  }
`;

const AddToCartButtonSection = styled.div`
  margin-top: auto;
`;

const AddToCartButton = styled.button`
  border: none;
  background: var(--primary-color);
  padding: 0.5em 0.5em;
  width: 100%;
  border-radius: 5px;
  font-size: 1.1em;
  &:disabled {
    opacity: 0.6;
  }
`;

const ProductTitle = styled.div`
  text-transform: capitalize;
`;

const PriceSection = styled.p`
  text-transform: capitalize;
  span {
    font-size: 1.8em;
    margin-right: 5px;
  }
`;

const Item = connect(state => ({ cart: state.cart || [] }))(({ item, cart }) => {
  const addItemToCart = item => () => {
    store.dispatch({ type: ADD_CART_TYPE, data: { name: item.itemName } });
  };
  const cartItem = findLast(cart, { name: item.itemName });
  return (
    <ItemOuter>
      <ItemInner>
        <div>
          <LazyImage src={item.imgSrc} alt={item.itemName} />
          <ProductTitle>{item.itemName}</ProductTitle>
        </div>
        <div>
          <PriceSection>
            <span>{formatCurrency(item.price)}</span> {item.quantityRemaining} In Stock
          </PriceSection>
        </div>
        <AddToCartButtonSection>
          <AddToCartButton disabled={item.quantityRemaining === 0 || cartItem} onClick={addItemToCart(item)}>
            {cartItem ? "In Cart" : item.quantityRemaining === 0 ? "Sold Out" : "Add to Cart"}
          </AddToCartButton>
        </AddToCartButtonSection>
      </ItemInner>
    </ItemOuter>
  );
});

const ItemList = ({ items = [] }) => (
  <ItemListContainerOuter>
    <ItemListContainer>
      {items.map(item => (
        <Item item={item} />
      ))}
    </ItemListContainer>
  </ItemListContainerOuter>
);

export default connect(state => ({
  items: state.items || [],
  cart: state.cart || []
}))(ItemList);
