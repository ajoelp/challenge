import React from "react";
import styled from "styled-components";
import { Container } from "./Layout";
import { store } from "../services/store";
import { OPEN_SIDEBAR } from "../reducers/sidebar_toggle";
import { ReactComponent as BagIcon } from "../svg/bag.svg";

const HeaderContainer = styled.div`
  box-shadow: 0 5px var(--primary-color);
`;

const NavMenu = styled.div`
  flex: 1;
  display: flex;
  align-items: flex-end;
  flex-direction: row;
  height: var(--header-height);
  button,
  a {
    color: white;
    text-decoration: none;
    background: var(--primary-color);
    font-size: 35px;
    font-weight: 1000;
    padding: 5px 35px;
    letter-spacing: 0.8px;
  }
`;

const CartButton = styled.button`
  display: none;
  @media only screen and (max-width: 875px) {
    display: flex;
    align-items: center;
    background: none;
    border: none;
    padding: 0 2em;
    cursor: pointer;
    svg {
      margin-right: 0.2em;
      height: 1.5em;
      width: auto;
      stroke: var(--primary-color);
    }
  }
`;

export const Header = props => {
  const openSidebar = () => {
    store.dispatch({ type: OPEN_SIDEBAR });
  };
  return (
    <HeaderContainer>
      <Container>
        <NavMenu>
          <a href="/">Fruit</a>
        </NavMenu>
        <CartButton onClick={openSidebar}>
          <BagIcon /> Cart
        </CartButton>
      </Container>
    </HeaderContainer>
  );
};
