import React, { useState } from "react";
import styled from "styled-components";

const Image = styled.img`
  min-height: 150px;
  min-width: 150px;
  &.image-loading {
    content: "";
    background: #efefef;
  }
  &.image-loaded {
    position: relative;
  }
`;

export const LazyImage = ({ src, alt }) => {
  const [loaded, setLoaded] = useState(false);
  const onLoad = e => {
    setLoaded(true);
  };
  const className = loaded ? "image-loaded" : "image-loading";
  return (
    <>
      <Image src={src} className={className} onLoad={onLoad} alt={alt} />
    </>
  );
};
