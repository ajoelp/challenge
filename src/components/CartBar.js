import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import find from "lodash/findLast";
import { store } from "../services/store";
import { REMOVE_CART_TYPE, DECREASE_CART_TYPE, EMPTY_CART_TYPE, INCREASE_CART_TYPE } from "../reducers/cart";
import reduce from "lodash/reduce";
import { formatCurrency } from "../services/utils";
import { UPDATE_ITEMS_DATA_TYPE } from "../reducers/items";
import { ReactComponent as CloseIcon } from "../svg/close.svg";
import { CLOSE_SIDEBAR } from "../reducers/sidebar_toggle";

const CartBarContainer = styled.div`
  flex: 0 0 100%;
  display: flex;
  flex-direction: column;
  background-color: var(--primary-color);
  flex: 0 0 250px;
  width: 250px;
  position: relative;
  @media only screen and (max-width: 875px) {
    display: block;
    position: fixed;
    top: 0;
    right: 0;
    height: 100%;
    transition: 0.2s cubic-bezier(0.175, 0.885, 0.32, 1) right;
    &.closed {
      transition: 0.2s cubic-bezier(0.175, 0.885, 0.32, 1) right;
      right: -100%;
    }
  }
`;

const CartBarCloseButton = styled.button`
  display: none;
  @media only screen and (max-width: 875px) {
    display: block;
    background: none;
    border: none;
    svg {
      path {
        fill: white;
      }
    }
  }
`;

const CartBarHeader = styled.div`
  color: white;
  padding: 0 1em;
  text-align: center;
  margin: 1em 0;
  h2 {
    margin: 0;
  }
  p {
    margin: 0;
  }
`;

const CartItemContainer = styled.div`
  background: rgba(255, 255, 255, 0.2);
  margin-bottom: 0.6em;
  padding: 0.5em 1em;
`;

const CartImage = styled.div`
  height: 60px;
  width: 60px;
  background: white;
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const QuantityContainer = styled.div`
  display: flex;
  align-items: flex-end;
  ${CartImage} {
    margin-right: 0.8em;
  }
`;

const QuantityPicker = styled.div`
  display: inline-flex;
  align-items: center;
  p {
    margin: 0 0.5em;
    font-size: 1.2em;
  }
  button {
    height: 15px;
    width: 15px;
    display: flex;
    padding: 0;
    margin: 0;
    background: white;
    border: none;
    align-items: center;
    justify-content: center;
  }
`;

const CartItemBottom = styled.div`
  display: flex;
  margin-top: 5px;
  button {
    margin-left: auto;
    background: none;
    color: white;
    border: none;
  }
  p {
    margin: 0;
  }
`;

const TotalContainer = styled.div`
  width: calc(100% - 2em);
  margin-left: 2em;
  border-top: 2px solid white;
  margin-top: 1em;
  text-align: right;
  padding: 0.5em 1em;
  p {
    margin: 0;
    font-weight: bold;
    color: white;
  }
  button:not(.main) {
    display: block;
    margin: 0;
    margin-left: auto;
    background: none;
    border: none;
    color: white;
    padding: 0;
  }
  button.main {
    background: rgba(255, 255, 255, 0.5);
    border: none;
    padding: 0.3em 0.8em;
    border-radius: 5px;
    display: block;
    margin-left: auto;
    margin-top: 0.5em;
    font-size: 1.1em;
    font-weight: bold;
  }
`;

const CartItem = props => {
  const item = find(props.items, { itemName: props.item.name });
  const deleteCartItem = () => {
    store.dispatch({ type: REMOVE_CART_TYPE, data: { name: props.item.name } });
  };
  const increaseCartItem = () => {
    store.dispatch({ type: INCREASE_CART_TYPE, data: { name: props.item.name } });
  };
  const decreaseCartItem = () => {
    store.dispatch({
      type: DECREASE_CART_TYPE,
      data: { name: props.item.name }
    });
  };
  return (
    <CartItemContainer>
      <QuantityContainer>
        <CartImage>
          <img src={item.imgSrc} alt={item.itemName} width={50} />
        </CartImage>
        <QuantityPicker>
          <button disabled={props.item.quantity === 1} onClick={decreaseCartItem}>
            -
          </button>
          <p>{props.item.quantity || 1}</p>
          <button disabled={props.item.quantity === item.quantityRemaining} onClick={increaseCartItem}>
            +
          </button>
        </QuantityPicker>
      </QuantityContainer>
      <CartItemBottom>
        <p>
          @{formatCurrency(item.price)} each = {formatCurrency(item.price * (props.item.quantity || 1))}
        </p>
        <button onClick={deleteCartItem}>Delete</button>
      </CartItemBottom>
    </CartItemContainer>
  );
};

const CartBarOuter = props => {
  const { cart, items } = props;
  const itemCount = reduce(
    cart,
    (result, value) => {
      return (value.quantity || 1) + result;
    },
    0
  );
  const itemsTotal = reduce(
    cart,
    (result, value) => {
      const dataItem = find(items, { itemName: value.name });
      const itemTotal = (value.quantity || 1) * dataItem.price;
      return result + itemTotal;
    },
    0
  );

  const emptyCart = () => {
    store.dispatch({ type: EMPTY_CART_TYPE });
  };

  const confirmPurchase = () => {
    // Update the values in the dataset
    store.dispatch({ type: UPDATE_ITEMS_DATA_TYPE, data: props.cart });
    // Clear the cart
    emptyCart();
  };

  const closeSidebar = () => {
    store.dispatch({ type: CLOSE_SIDEBAR });
  };

  return (
    <CartBarContainer className={props.sidebar_toggle ? "open" : "closed"}>
      <CartBarHeader>
        <CartBarCloseButton onClick={closeSidebar}>
          <CloseIcon />
        </CartBarCloseButton>
        <h2>Shopping Cart</h2>
        <p>{itemCount} Items</p>
      </CartBarHeader>
      <div>
        {cart.map(cartItem => (
          <CartItem items={props.items} item={cartItem} />
        ))}
      </div>
      <TotalContainer>
        <p>Total: {formatCurrency(itemsTotal)}</p>
        <button onClick={emptyCart}>Empty Cart</button>
        <button className="main" onClick={confirmPurchase} disabled={cart.length <= 0}>
          Confirm Purchase
        </button>
      </TotalContainer>
    </CartBarContainer>
  );
};

export const CartBar = connect(state => ({
  cart: state.cart || [],
  items: state.items || [],
  sidebar_toggle: state.sidebar_toggle || false
}))(CartBarOuter);
