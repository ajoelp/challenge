import React from 'react';
import ItemList from '../components/ItemList';
import { Header } from '../components/Header';
import { Container } from '../components/Layout';
import { CartBar } from '../components/CartBar';

export default class HomeScreen extends React.Component {
  render() {
    return (
      <>
        <Header />
        <Container>
          <ItemList />
          <CartBar />
        </Container>
      </>
    );
  }
}
