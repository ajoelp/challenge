import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { createGlobalStyle } from "styled-components";

import * as serviceWorker from "./serviceWorker";
import HomeScreen from "./screens/Home";
import { store, persistor } from "./services/store";

const GlobalStyles = createGlobalStyle`
  :root{
    --primary-color: #32c996;
    --header-height: 100px;
  }
  #root{
    min-height: 100vh;
    display: flex; flex-direction: column;
  }
  body{
    padding: 0;
    margin: 0;
    background-color: white;
    font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol
  }
  * {
     box-sizing: border-box;
  }
`;

const App = () => (
  <Provider store={store}>
    <GlobalStyles />
    <PersistGate loading={null} persistor={persistor}>
      <HomeScreen />
    </PersistGate>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
