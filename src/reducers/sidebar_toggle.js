export const CLOSE_SIDEBAR = "@sidebar/close";
export const OPEN_SIDEBAR = "@sidebar/open";
export const sidebar_toggle = (state = false, action) => {
  switch (action.type) {
    case CLOSE_SIDEBAR:
      return false;
    case OPEN_SIDEBAR:
      return true;
    default:
      return state;
  }
};
