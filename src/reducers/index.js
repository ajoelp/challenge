import { combineReducers } from "redux";
import { cart } from "./cart";
import { items } from "./items";
import { sidebar_toggle } from "./sidebar_toggle";

export const rootReducer = combineReducers({
  cart,
  items,
  sidebar_toggle
});
