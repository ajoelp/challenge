import map from "lodash/map";
import findLast from "lodash/findLast";

export const UPDATE_ITEMS_DATA_TYPE = "@items/update";

export const items = (state = [], action) => {
  switch (action.type) {
    case UPDATE_ITEMS_DATA_TYPE:
      return map(state, a => {
        const cartItem = findLast(action.data, { name: a.itemName });
        return cartItem ? { ...a, quantityRemaining: a.quantityRemaining - (cartItem.quantity || 1) } : a;
      });
    default:
      return state;
  }
};
