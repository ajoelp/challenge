import filter from "lodash/filter";
import findLast from "lodash/findLast";
import map from "lodash/map";

export const ADD_CART_TYPE = "@cart/add";
export const REMOVE_CART_TYPE = "@cart/delete";
export const DECREASE_CART_TYPE = "@cart/decrease";
export const INCREASE_CART_TYPE = "@cart/increase";
export const EMPTY_CART_TYPE = "@cart/empty";

export const cart = (state = [], action) => {
  switch (action.type) {
    case ADD_CART_TYPE:
      const item = findLast(state, { name: action.data.name });
      return !item ? [...state, action.data] : state;
    case DECREASE_CART_TYPE:
      return map(state, a => (a.name === action.data.name ? { name: a.name, quantity: (a.quantity || 1) - 1 } : a));
    case INCREASE_CART_TYPE:
      return map(state, a => (a.name === action.data.name ? { name: a.name, quantity: (a.quantity || 1) + 1 } : a));
    case REMOVE_CART_TYPE:
      return filter(state, a => a.name !== action.data.name);
    case EMPTY_CART_TYPE:
      return [];
    default:
      return state;
  }
};
